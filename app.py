import logging
import re

from flask import Flask, make_response
from slack import WebClient
from slackeventsapi import SlackEventAdapter
import ssl as ssl_lib
import certifi
import configparser

from handlers.rule_based_handler import RuleBaseHandler
from handlers.song_lyrics_handler import SongLyricsHandler
from handlers.news_handler import NewsHandler

config = configparser.ConfigParser()
config.read('config.ini')
# Initialize a Flask app to host the events adapter
app = Flask(__name__)
slack_events_adapter = SlackEventAdapter(config['SLACK']['SlackSigningSecret'], "/slack/events", app)

# Initialize a Web API client
slack_web_client = WebClient(token=config['SLACK']['SlackBotToken'])
queue = []


@slack_events_adapter.on("message")
def message(payload):
    event = payload.get("event", {})
    # thumbsup reaction for files & images
    if (event.get("files")):
        slack_web_client.reactions_add(
            channel=event.get("channel"),
            name="thumbsup",
            timestamp=event.get("ts")
        )


@slack_events_adapter.on("app_mention")
def app_mention(payload):
    event = payload.get("event", {})
    message_id = event.get("client_msg_id")
    print(event)

    if (message_id in queue):
        return

    else:
        queue.append(message_id)

        channel_id = event.get("channel")
        message = normalize_message(event.get("text"))

        make_response("", 200)

        if message_contains(message, ["co słychać", "co tam słychać", "co slychac", "co tam slychac", "co ciekawego"]):
            slack_web_client.chat_postMessage(channel=channel_id, text="co słychać? Już mówię...")
            news_handler = NewsHandler(message)
            response = news_handler.response()

        elif message_contains(message, ['slowa do ', 'słowa do ', 'tekst do ']):
            slack_web_client.chat_postMessage(channel=channel_id, text="szukam... :male-detective:")
            song_lyrics_handler = SongLyricsHandler(message)
            response = song_lyrics_handler.get()

        else:
            rule_based_handler = RuleBaseHandler(message)
            response = rule_based_handler.respond()

        slack_web_client.chat_postMessage(channel=channel_id, text=response)
        queue.remove(message_id)


def normalize_message(message: str):
    return re.sub(r'<@.+> ', '', message.lower())


def message_contains(message: str, commandList: list):
    for command in commandList:
        if re.match(".*" + command, message, re.I):
            return 1
    return 0


if __name__ == "__main__":
    logger = logging.getLogger()
    logger.setLevel(logging.DEBUG)
    logger.addHandler(logging.StreamHandler())
    ssl_context = ssl_lib.create_default_context(cafile=certifi.where())
    app.run(port=3000)

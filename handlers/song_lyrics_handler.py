from lyrics_extractor import Song_Lyrics
import configparser

config = configparser.ConfigParser()
config.read('config.ini')

class SongLyricsHandler:

    query: str

    def __init__(self, message):
        self.query = message

    def get(self):
        get_song_name = self.query[9:]
        lyrics_gen = Song_Lyrics(config["GCS"]["GCSApiKey"], config["GCS"]["GCSEngineId"])
        song = lyrics_gen.get_lyrics(get_song_name)
        return '*' + song[0] + '*' + '\n\n' + song[1].replace('<br>', '\n')
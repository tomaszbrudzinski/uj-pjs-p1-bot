#Slack Bot

Simple slack bot which provides:
- rule-based responses
- song lyrics service
- news service

# Usage

##Prerequisites
Create slack app: https://github.com/slackapi/python-slackclient/blob/master/tutorial/01-creating-the-slack-app.md

##1. Prepare config:
- `cp config.EXAMPLE.ini config.ini` and provide API credentials

##2. Create virtual environment

##3. Run app
- `python3 app.py`


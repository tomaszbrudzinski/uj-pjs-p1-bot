import requests
import configparser
import logging

config = configparser.ConfigParser()
config.read('config.ini')


class NewsHandler:
    query: str

    def __init__(self, message: str):
        self.query = message

    def response(self):

        query_url = self.prepare_query_url()
        news = requests.get(query_url)
        data = news.json()

        max_range = 3 if len(data["articles"]) > 3 else len(data["articles"])
        if max_range:
            get_news = ''
            for i in range(max_range):
                title = data["articles"][i]["title"]
                description = data["articles"][i]["description"]
                link = data["articles"][i]["url"]
                short_link = self.shorten_url(link)
                get_news += "*" + title + " :* _" + description + "_ \n" + short_link + "\n\n"
            return get_news

        else:
            return "...a wiesz co, nic ciekawego :wink:"

    def prepare_query_url(self):

        query_topic = None

        if self.query.find(" w "):
            query_topic = self.query.partition(" w ")[2]

        if self.query.find(" o "):
            query_topic = self.query.partition(" o ")[2]

        if query_topic:
            return "https://newsapi.org/v2/top-headlines?country=pl&q=" + query_topic + "&apiKey=" + config["NEWSAPI"][
                "NewsApiKey"]
        else:
            return "https://newsapi.org/v2/top-headlines?country=pl&apiKey=" + config["NEWSAPI"]["NewsApiKey"]

    def shorten_url(self, uri):
        query_params = {
            'access_token': config["BITLY"]["BitlyAccesToken"],
            'longUrl': uri
        }

        endpoint = 'https://api-ssl.bitly.com/v3/shorten'
        response = requests.get(endpoint, params=query_params, verify=False)

        data = response.json()

        if not data['status_code'] == 200:
            logger = logging.getLogger()
            logger.error("Unexpected status_code: {} in bitly response. {}".format(data['status_code'], response.text))

        return data['data']['url']

from difflib import SequenceMatcher


class RuleBaseHandler:

    query: str

    SIMILARITY_THRESHOLD = 0.2

    QUERIES = (
        "przedstaw się",
        "czesc",
        "siema mordo",
        "zagramy w planszówkę",
        "sesja rpg",
        "słucha muzyki",
        "idziemy w plener",
        "prawda co nie",
        "zajebiście"
    )

    REPLIES = (
        "Siemano Panowie! Kopę lat :sunglasses:",
        "no powitać :P",
        "siemano :smile:",
        "może innym razem, ale w rpg to bym sobie zagrał",
        "jasne! Jestem wolny w czwartek, zdzwońmy się",
        "pamiętacie Stagnację? Kurłła... kiedyś to było...",
        "to ja ogarnę napoje i wezmę cajon, niech ktoś wezmie gitarę",
        "no jasne, że tak",
        "hehe"
    )

    def __init__(self, message: str):
        self.query = message

    def respond(self):
        max_similarity = 0
        for query_id in range(len(self.QUERIES)):
            similarity = SequenceMatcher(None, self.query, self.QUERIES[query_id]).ratio()
            if similarity > max_similarity:
                highest_index = query_id
                max_similarity = similarity

        if max_similarity > self.SIMILARITY_THRESHOLD:
            return self.REPLIES[highest_index]
        else:
            return "Nie mam, nie wiem, nie znam się, zarobiony jestem. Kombinuj."
